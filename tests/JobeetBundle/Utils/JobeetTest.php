<?php 


 
namespace Tests\JobeetBundle\Utils;
use JobeetBundle\Utils\Jobeet;
use PHPUnit\Framework\TestCase;
 
class JobeetTest extends TestCase
{
  public function testSlugify()
  {
    $this->assertEquals('sensio', Jobeet::slugify('Sensio'));
    $this->assertEquals('sensio-labs', Jobeet::slugify('sensio labs'));
    $this->assertEquals('sensio-labs', Jobeet::slugify('sensio   labs'));
    $this->assertEquals('paris-france', Jobeet::slugify('paris,france'));
    $this->assertEquals('dev', Jobeet::slugify('  dév'));
    $this->assertEquals('sensio', Jobeet::slugify('sensio  '));
  }
}