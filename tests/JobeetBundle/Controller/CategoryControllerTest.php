<?php 

namespace Tests\JobeetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryControllerTest extends WebTestCase
{

	public function testShow(){

		$client = static::createClient();
		$crawler = $client->request('GET', '/jobbet/categories');
		$this->assertEquals('JobeetBundle\Controller\CategoryController::indexAction', $client->getRequest()->attributes->get('_controller'));

    $this->assertTrue(200 === $client->getResponse()->getStatusCode());

	}
}