<?php

namespace JobeetBundle\Controller;

use JobeetBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use JobeetBundle\Service\FileUploader;

/**
 * Job controller.
 *
 */
class JobController extends Controller
{
    /**
     * Lists all job entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $jobs = $em->getRepository('JobeetBundle:Job')->getActiveJobs();
        $categories = $em->getRepository('JobeetBundle:Category')->getWithJobs();
 
        foreach($categories as $category)
        {
            $category->setActiveJobs($em->getRepository('JobeetBundle:Job')->getActiveJobs($category->getId(),10));
        }

        return $this->render('@Jobeet/job/homepage.html.twig', array(
            'jobs' => $jobs,
            'categories' => $categories
        ));
    }

    /**
     * Creates a new job entity.
     *
     */
    public function newAction(Request $request, FileUploader $fileUploader)
    {
        $job = new Job();
        $form = $this->createForm('JobeetBundle\Form\JobType', $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $file = $job->getLogo();
            $fileName = $fileUploader->upload($file);

            $job->setLogo($fileName);
            
            $em->persist($job);
               
            $em->flush();

            return $this->redirectToRoute('ens_job_preview', array(
                'company' => $job->getCompanySlug(),
                'location' => $job->getLocationSlug(),
                'token' => $job->getToken(), 
                'position' => $job->getPositionSlug()
            ));
        }

        return $this->render('@Jobeet/job/new.html.twig', array(
            'job' => $job,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a job entity.
     *
     */
    public function showAction(Job $job)
    {

        $em = $this->getDoctrine()->getManager();
        
        $result = $em->getRepository('JobeetBundle:Job')->getActiveJob($job->getId());
        

        return $this->render('@Jobeet/job/job-single.html.twig', array(
            'job' => $result,
        ));
    }

    /**
     * Preview a job entity.
     *
     */
    public function previewAction($token)
    {

        $em = $this->getDoctrine()->getManager();
        
        $result = $em->getRepository('JobeetBundle:Job')->findOneByToken($token);

        if (!$result) {
            throw new createNotFoundException("Offre introuvable");
            
        }
        

        return $this->render('@Jobeet/job/job-single.html.twig', array(
            'job' => $result,
        ));
    }

    /**
     * Extend expire date of a job entity.
     *
     */
    public function extendAction($token)
    {

        $em = $this->getDoctrine()->getManager();
        
        $job = $em->getRepository('JobeetBundle:Job')->findOneByToken($token);

        if (!$job) 
            throw new createNotFoundException("Offre introuvable");

        if ($job->extend()) {
            $em->flush();
            $this->addFlash('success', sprintf('La date de validité de votre annonce a été étendu jusqu au %s.', $job->getExpiresAt()->format('m/d/Y')));
        }
        
            
        return $this->redirectToRoute('ens_job_preview', array(
            'company' => $job->getCompanySlug(),
            'location' => $job->getLocationSlug(),
            'token' => $job->getToken(), 
            'position' => $job->getPositionSlug()
        ));
        

        
    }

    /**
     * Displays a form to edit an existing job entity.
     *
     */
    public function editAction(Request $request, $token)
    {
        //$deleteForm = $this->createDeleteForm($job);
        $em = $this->getDoctrine()->getManager();

        $job = $em->getRepository('JobeetBundle:Job')->findOneByToken($token);

        if (!$job) 
            throw new createNotFoundException("Offre introuvable");
            
        

        $editForm = $this->createForm('JobeetBundle\Form\JobType', $job);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            return $this->redirectToRoute('ens_job_preview', array(
                'company' => $job->getCompanySlug(),
                'location' => $job->getLocationSlug(),
                'token' => $job->getToken(), 
                'position' => $job->getPositionSlug()
            ));
        }

        return $this->render('job/edit.html.twig', array(
            'job' => $job,
            'form' => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a job entity.
     *
     */
    public function deleteAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();

        $job = $em->getRepository('JobeetBundle:Job')->findOneByToken($token);

        if (!$job) 
            throw new createNotFoundException("Offre introuvable");

        $em->remove($job);
        $em->flush();
        

        return $this->redirectToRoute('ens_job_index');
    }

    /**
     * Creates a form to delete a job entity.
     *
     * @param Job $job The job entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Job $job)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ens_job_delete', array('id' => $job->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Publish job.
     *
     * @param Job $job The job entity
     */

    public function publishAction(Request $request, $token){
        $em = $this->getDoctrine()->getManager();

        $job = $em->getRepository('JobeetBundle:Job')->findOneByToken($token);

        if (!$job) 
            throw new createNotFoundException("Offre introuvable");

        $job->setIsActivated(true);
        $em->flush();

        $this->addFlash(
            'success',
            'Annonce publiée, elle sera active pendant 30 jours'
        );
        return $this->redirectToRoute('ens_job_preview', array(
                'company' => $job->getCompanySlug(),
                'location' => $job->getLocationSlug(),
                'token' => $job->getToken(), 
                'position' => $job->getPositionSlug()
            ));


    }
}
