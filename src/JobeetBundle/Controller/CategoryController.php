<?php

namespace JobeetBundle\Controller;

use JobeetBundle\Entity\Category;
use JobeetBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Category controller.
 *
 */
class CategoryController extends Controller
{
    /**
     * Lists all categories entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('JobeetBundle:Category')->findAll();

        return $this->render('@Jobeet/category/showJobsWithFilter.html.twig', array(
            'categories' => $categories
        ));
    }

    public function getJobsByCategoryAction(Request $request){

       if ($request->isXMLHttpRequest()) {
            $cat_id = $request->get('category_id');
            $em = $this->getDoctrine()->getManager();
            $jobs = $em->getRepository('JobeetBundle:Job')->getActiveJobs($cat_id);

            $results =[];
            foreach ($jobs as $job) {
               array_push($results, [
                'id' => $job->getId(),
                'position' => $job->getPosition(),
                'location' => $job->getLocation(),
                'typeJ' => $job->getType(),
                'company' => $job->getCompany(),
                'positionSlug' => $job->getPositionSlug(),
                'companySlug' => $job->getCompanySlug(),
                'locationSlug' => $job->getLocationSlug()

            ]);
            }

            // var_dump($results); 

            return new JsonResponse([ 'jobs' =>$results]);
       }

        return new Response('error');
        



    }

    

   
  
}
