<?php

namespace JobeetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use JobeetBundle\Entity\Job; 
use JobeetBundle\Entity\Category; 
use JobeetBundle\Form\CategoryType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
class JobType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('type',ChoiceType::class, [
            'choices' => Job::getTypes(),
            'expanded' => true
        ])
        ->add('company', null, ['label' => 'Société'])
        // ->add('logo', null, ['label' => 'logo de la société'])        ->add('url')
        ->add('position')
        ->add('location')
        ->add('description',TextareaType::class,['attr' => array('cols' => '30', 'rows' => '5')])
        ->add('howtoApply',TextareaType::class,[
            'label' => 'Comment postuler ?',
            'attr' => array('cols' => '30', 'rows' => '5')
        ])
        // ->add('token')
        ->add('isPublic' ,null, ['label' => 'Est publique ?'])
        ->add('email')
        ->add('category', EntityType::class, [
            'class' => 'JobeetBundle:Category',
            'choice_label' => 'name'
        ])
        ->add('logo', FileType::class, array('label' => 'logo de la société', 'required' => false));
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JobeetBundle\Entity\Job'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jobeetbundle_job';
    }


}
