<?php

namespace JobeetBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class JobAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->add('category')
        ->add('type', ChoiceType::class, array('choices' => Job::getTypes(), 'expanded' => true))
        ->add('company')
        ->add('file', FileType::class, array('label' => 'Logo', 'required' => false))
        ->add('url')
        ->add('position')
        ->add('location')
        ->add('description')
        ->add('howtoApply')
        ->add('isPublic')
        ->add('email')
        ->add('isActivated')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        ->add('category')
        ->add('company')
        ->add('url')
        ->add('position')
        ->add('description')
        ->add('isPublic')
        ->add('email')
        ->add('isActivated')
        ->add('expiresAt');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        ->addIdentifier('company')
            ->add('position')
            ->add('location')
            ->add('url')
            ->add('isActivated')
            ->add('email')
            ->add('category')
            ->add('expiresAt')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }
}