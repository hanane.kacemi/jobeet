// JSRoutingBundle configuration


// const routes = require('../../web/js/fos_js_routes.json');
// import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

// Routing.setRoutingData(routes);
// Routing.generate('rep_log_list');


// End JSRoutingBundle configuration


jQuery(document).ready(function($){
	// page categorie, when category is selected we load active jobs that belongs to this category
	$('#categories').change(function(){
		$('#selectedCategory').html($(this).find("option:selected").text());
		let category_id = this.value;

		// let route = Routing.generate('ens_category_page','',true);
		
		$.ajax({
			url : "http://localhost:8888/jobeet/web/app_dev.php/filter-jobs-par-categorie",
			type: "POST",
			data : {
				'category_id' : category_id},
			dataType: "json",
			 success: function(result){
			 	$('.jobs-loaded-by-js').html('');
			 	
			 	if (result.jobs.length > 0 ) {
			 		let job ;
			 		
			 		for (let i = 0; i<result.jobs.length;  i++) {

			 			job = result.jobs[i];
			 			console.log(job);
			 			$('.jobs-loaded-by-js').append('\
			 				<div class="row" data-aos="fade">\
						         <div class="col-md-12">\
						           <div class="job-post-item bg-white p-4 d-block d-md-flex align-items-center">\
						              <div class="mb-4 mb-md-0 mr-5">\
						               <div class="job-post-item-header d-flex align-items-center">\
						                 <h2 class="mr-3 text-black h4">'+ job.position +'</h2>\
						                 <div class="badge-wrap">\
						                  <span class="bg-warning text-white badge py-2 px-4">' + job.typeJ + '</span>\
						                 </div>\
						               </div>\
						               <div class="job-post-item-body d-block d-md-flex">\
						                 <div class="mr-3"><span class="fl-bigmug-line-portfolio23"></span> <a href="#">'+ job.company + '</a></div>\
						                 <div><span class="fl-bigmug-line-big104"></span> <span>'+ job.location +'</span></div>\
						               </div>\
						              </div>\
						              <div class="ml-auto">\
						                <a href="http://localhost:8888/jobeet/web/app_dev.php/job/' + job.companySlug + '/' + job.locationSlug + '/' + job.id  + '/'+ job.positionSlug + '" class="btn btn-primary py-2">Postuler</a>\
						              </div>\
						           </div>\
						         </div>\
				        </div>');
			 		}//for
			 	}//
			 	else 
			 		$('.jobs-loaded-by-js').html('Aucune offre trouvée');
			 	

			 }

		});
	});

	$('#categories').trigger('change');

});

